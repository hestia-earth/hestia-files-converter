# Hestia Files Converter

Scripts to convert files into the Hestia JSON-Node format

## Install

```bash
pip install hestia_earth.converter
```

### Usage

```python
from hestia_earth.converter import Converter, NodeType

converter = Converter('./output')
converter.convert(NodeType.Cycle, 'path/to/file.xlsx')
```
