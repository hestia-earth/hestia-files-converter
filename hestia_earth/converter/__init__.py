from .file_utils import mkdirs
from .types import NodeType
from .excel import convert


class Converter:
    def __init__(self, output_dir: str):
        mkdirs(output_dir)
        self.output_dir = output_dir

    def convert(self, node_type: NodeType, filepath: str, *args):
        return convert(self.output_dir, node_type, filepath, *args)
