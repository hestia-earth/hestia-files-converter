import secrets
import json
from collections import OrderedDict
import pandas as pd
import os
import math


from .types import NodeType


class ComplexEncoder(json.JSONEncoder):
    def default(self, obj):
        if hasattr(obj, 'reprJSON'):
            return obj.reprJSON()
        else:
            return json.JSONEncoder.default(self, obj)


class Schema():
    terms = {}
    sources = {}
    bibliographies = {}
    cycles = {}
    sites = {}

    def __init__(self):
        self.fields = OrderedDict()
        self.map = {}

    def add_object(self, root, keylist):
        pass

    def build_map(self, root_schema_objects_dict, columns):
        for column in columns:
            if not self.isnan(column):
                self.map[column] = self.get_store_object(root_schema_objects_dict[column], column.split("."))

    def get_store_object(self, root, keyslist):
        if len(keyslist) == 1:
            object = self

        if len(keyslist) > 1:
            object = self.add_object(root, keyslist)

        return object

    def clean(self, raw_json):
        clean_fields = json.JSONDecoder(object_pairs_hook=OrderedDict).decode(raw_json)
        return self.clean_crawler_fields(self.clean_crawler_links(self.clean_crawler_fields(clean_fields)))

    def clean_crawler_fields(self, mobject):
        clean_fields = mobject
        for key in list(clean_fields.keys()):
            if isinstance(clean_fields[key], list) and not clean_fields[key] == []:
                for object in clean_fields[key]:
                    if not isinstance(object, str) and not isinstance(object, int) and not isinstance(object, float):
                        self.clean_crawler_fields(object)
            elif isinstance(clean_fields[key], dict) and not clean_fields[key] == {}:
                self.clean_crawler_fields(clean_fields[key])
            else:
                if clean_fields[key] == "" or clean_fields[key] == None or clean_fields[key] == [] or clean_fields[
                        key] == "-":
                    del (clean_fields[key])
        return clean_fields

    def clean_crawler_links(self, mobject):
        clean_fields = mobject
        if isinstance(clean_fields, dict):
            for key in list(clean_fields.keys()):
                if isinstance(clean_fields[key], list) and not clean_fields[key] == []:
                    index = 0
                    while index < len(clean_fields[key]):
                        if self.check_empty_link(clean_fields[key][index]):
                            del (clean_fields[key][index])
                        else:
                            self.clean_crawler_links(clean_fields[key][index])
                            index += 1
                elif isinstance(clean_fields[key], dict) and not clean_fields[key] == {}:
                    if self.check_empty_link(clean_fields[key]):
                        del (clean_fields[key])
                    else:
                        self.clean_crawler_links(clean_fields[key])

        return clean_fields

    def check_empty_link(self, mobject):
        contition_1 = False
        contition_2 = False
        contition_3 = False
        contition_4 = False

        if isinstance(mobject, dict):
            if len(list(mobject.keys())) == 2:
                contition_1 = True
            else:
                return False
            for key in list(mobject.keys()):
                if key == "type":
                    contition_2 = True
                else:
                    if isinstance(mobject[key], dict):
                        for subkey in mobject[key]:
                            if subkey == "id":
                                contition_3 = True
                            if subkey == "name":
                                contition_4 = True

                    else:
                        return False
        else:
            return False

        if contition_1 and contition_2 and contition_3 and contition_4:
            return True
        else:
            return False

    def compare(self, obj_a, obj_b):
        if obj_a.fields == obj_b.fields:
            return True
        else:
            return False

    def get_reference(self):
        reference = Ref()
        if "id" in self.fields.keys():
            reference.fields["id"] = self.fields["id"]
        if "type" in self.fields.keys():
            reference.fields["type"] = self.fields["type"]
        if "name" in self.fields.keys():
            reference.fields["name"] = self.fields["name"]
        if "units" in self.fields.keys():
            reference.fields["units"] = self.fields["units"]

        return reference

    def get_unique_identifier(self, record):
        number = secrets.token_urlsafe(3)
        while ("_" in number or "-" in number or number in record):
            number = secrets.token_urlsafe(3)
        record.append(number)
        return number

    def get_measurement(self):
        measurement = Measurement()
        measurement.fields["term"] = self.get_reference()
        return measurement

    def get_property(self):
        property = Property()
        property.fields["term"] = self.get_reference()
        return property

    def isnan(self, value):
        if isinstance(value, str):
            return False
        else:
            if not math.isnan(value):
                return False
            else:
                return True

    def keys(self):
        return self.fields.keys()

    def reprJSON(self):
        return self.fields

    def update_value(self, keysstream, value):
        keyslist = keysstream.split(".")
        key = keyslist[-1]
        object_to_update = self.map[keysstream]

        if key == "term":
            object_to_update.fields[key] = Schema.terms[value].get_reference()

        if key == "key":
            if not value == "-":
                object_to_update.fields[key] = Schema.terms[value].get_reference()

        else:
            if not self.isnan(value):
                if key in object_to_update.fields.keys():
                    if isinstance(object_to_update.fields[key], list):
                        object_to_update.fields[key].append(value)
                    elif isinstance(object_to_update.fields[key], str):
                        object_to_update.fields[key] = str(value)
                    else:
                        object_to_update.fields[key] = value

    def write(self):
        processed_json = self.clean(json.dumps(self.reprJSON(), indent=2, cls=ComplexEncoder))
        return json.dumps(processed_json, indent=2, cls=ComplexEncoder)


class Term(Schema):
    record = []

    def __init__(self, termtype):
        super().__init__()
        self.fields["type"] = "Term"
        self.fields["id"] = termtype + "/" + self.get_unique_identifier(Term.record)
        self.fields["termType"] = termtype
        self.fields["name"] = ""
        self.fields["shortName"] = ""
        self.fields["synonyms"] = []
        self.fields["definition"] = ""
        self.fields["units"] = ""
        self.fields["defaultProperties"] = []
        self.fields["source"] = None
        self.fields["dependentVariables"] = []
        self.fields["independentVariables"] = []

    def add_object(self, root, keyslist):
        if keyslist[0] in list(Schema.terms.keys()):
            property = Schema.terms[keyslist[0]].get_property()
            self.fields["defaultProperties"].append(property)
            if not keyslist[1].isnumeric():
                return property.get_store_object(root, keyslist[1:])
            else:
                return property.get_store_object(root, keyslist[2:])

    def format(self):
        if self.fields["synonyms"] == ["-"]:
            self.fields["synonyms"] = []
        if not self.fields["synonyms"] == []:
            self.fields["synonyms"] = list(
                map(lambda x: x.strip(), self.fields["synonyms"][0].replace(",", ";").split(';')))

    def update_schema(self):
        Schema.terms[self.fields["shortName"]] = self
        Schema.terms[self.fields["name"]] = self
        Schema.terms[self.fields["id"]] = self
        return [self]


class Property(Schema):
    def __init__(self):
        super().__init__()
        self.fields["type"] = "Property"
        self.fields["term"] = None
        self.fields["key"] = None
        self.fields["value"] = ""
        self.fields["amount"] = None
        self.fields["sd"] = ""
        self.fields["source"] = ""


class Cycle(Schema):
    record = []

    def __init__(self):
        super().__init__()
        self.fields["type"] = "Cycle"
        self.fields["id"] = "cycle/" + self.get_unique_identifier(Cycle.record)
        self.fields["name"] = ""
        self.fields["site"] = None
        self.fields["defaultSource"] = None
        self.fields["startDate"] = ""
        self.fields["endDate"] = ""
        self.fields["treatment"] = ""
        self.fields["treatmentDescription"] = ""
        self.fields["functionalUnitMeasure"] = ""
        self.fields["functionalUnitDetails"] = ""
        self.fields["inputs"] = []
        self.fields["emissions"] = []
        self.fields["products"] = []
        self.fields["dataPrivate"] = None

        self.source = None
        self.site = None
        self.inputs = []
        self.inputs_dict = {}
        self.products = []
        self.products_dict = {}
        self.emissions = []
        self.emissions_dict = {}

    def add_object(self, root, keyslist):

        if keyslist[0] == "source":
            if self.source is None:
                source = Source()
                self.source = source
                self.fields["defaultSource"] = source.get_reference()
                return source.get_store_object(root, keyslist[1:])
            else:
                return self.source.get_store_object(root, keyslist[1:])

        elif keyslist[0] == "site":
            if self.site is None:
                site = Site()
                self.site = site
                self.fields["site"] = site.get_reference()
                return self.site.get_store_object(root, keyslist[1:])
            else:
                return self.site.get_store_object(root, keyslist[1:])

        else:
            if keyslist[0] == "input":
                if keyslist[1] in self.inputs_dict.keys():
                    input = self.inputs_dict[keyslist[1]]
                    return input.get_store_object(root, keyslist[2:])
                else:
                    input = Input()
                    self.inputs.append(input)
                    self.inputs_dict[keyslist[1]] = input
                    self.fields["inputs"].append(input)
                    if not keyslist[1].isnumeric():
                        input.fields["term"] = Schema.terms[keyslist[1]].get_reference()
                        return input.get_store_object(root, keyslist[2:])
                    else:
                        return input.get_store_object(root, keyslist[2:])

            if keyslist[0] == "emission":
                if keyslist[1] in self.emissions_dict.keys():
                    emission = self.emissions_dict[keyslist[1]]
                    return emission.get_store_object(root, keyslist[2:])
                else:
                    emission = Emission()
                    self.emissions.append(emission)
                    self.emissions_dict[keyslist[1]] = emission
                    self.fields["emissions"].append(emission)
                    if not keyslist[1].isnumeric():
                        emission.fields["term"] = Schema.terms[keyslist[1]].get_reference()
                        return emission.get_store_object(root, keyslist[2:])
                    else:
                        return emission.get_store_object(root, keyslist[2:])

            if keyslist[0] == "product":
                if keyslist[1] in self.products_dict.keys():
                    product = self.products_dict[keyslist[1]]
                    return product.get_store_object(root, keyslist[2:])
                else:
                    product = Product()
                    self.products.append(product)
                    self.products_dict[keyslist[1]] = product
                    self.fields["products"].append(product)
                    if not keyslist[1].isnumeric():
                        product.fields["term"] = Schema.terms[keyslist[1]].get_reference()
                        return product.get_store_object(root, keyslist[2:])
                    else:
                        return product.get_store_object(root, keyslist[2:])

    def update_schema(self):
        Schema.cycles[self.fields["name"]] = self
        new_objects = []
        if not self.source is None:
            object = self.source.check_duplicate()
            if object is None:
                new_objects.extend(self.source.update_schema())
            else:
                self.fields["defaultSource"] = object.get_reference()
        if not self.site is None:
            object = self.site.check_duplicate()
            if object is None:
                new_objects.extend(self.site.update_schema())
            else:
                self.fields["site"] = object.get_reference()
        new_objects.extend([self])
        return new_objects

    def format(self):
        if self.fields["name"] == "":
            self.fields["name"] = self.fields["treatment"] + " - " + \
                self.fields["products"][0].fields["term"].fields["name"] + " - " + str(self.fields["startDate"])
            self.site.format()
            self.fields["site"] = self.site.get_reference()
            self.source.format()
            self.fields["defaultSource"] = self.source.get_reference()


class Input(Schema):
    def __init__(self):
        super().__init__()
        self.fields["type"] = "Input"
        self.fields["term"] = None
        self.fields["description"] = ""
        self.fields["amount"] = []
        self.fields["relDays"] = []
        self.fields["sd"] = None
        self.fields["sdDefinition"] = ""
        self.fields["currency"] = ""
        self.fields["price"] = None
        self.fields["cost"] = None
        self.fields["properties"] = []
        self.fields["method"] = None
        self.fields["methodDescription"] = ""
        self.fields["source"] = None
        self.fields["inventory"] = None

        self.source = None

    def add_object(self, root, keyslist):

        if keyslist[0] == "source":
            if self.source is None:
                self.source = Source()
                self.fields["source"] = self.source.get_reference()

            if not keyslist[1].isnumeric():
                return self.source.get_store_object(root, keyslist[1:])
            else:
                return self.source.get_store_object(root, keyslist[2:])

        if keyslist[0] == "property":
            if keyslist[1] in list(Schema.terms.keys()):
                property = Schema.terms[keyslist[1]].get_property()
                self.fields["properties"].append(property)

                if not keyslist[2].isnumeric():
                    return property.get_store_object(root, keyslist[2:])
                else:
                    return property.get_store_object(root, keyslist[3:])

        if keyslist[0] in list(Schema.terms.keys()):
            property = Schema.terms[keyslist[0]].get_property()
            self.fields["properties"].append(property)

            if not keyslist[1].isnumeric():
                return property.get_store_object(root, keyslist[1:])
            else:
                return property.get_store_object(root, keyslist[2:])


class Product(Schema):
    def __init__(self):
        super().__init__()
        self.fields["type"] = "Product"
        self.fields["term"] = None
        self.fields["description"] = ""
        self.fields["destination"] = ""
        self.fields["amount"] = []
        self.fields["relDays"] = []
        self.fields["sd"] = None
        self.fields["sdDefinition"] = ""
        self.fields["currency"] = ""
        self.fields["price"] = None
        self.fields["revenue"] = None
        self.fields["valueShare"] = None
        self.fields["properties"] = []
        self.fields["method"] = None
        self.fields["methodDescription"] = ""
        self.fields["source"] = None

        self.source = None

    def add_object(self, root, keyslist):

        if keyslist[0] == "source":
            if self.source is None:
                self.source = Source()
                self.fields["source"] = self.source.get_reference()

            if not keyslist[1].isnumeric():
                return self.source.get_store_object(root, keyslist[1:])
            else:
                return self.source.get_store_object(root, keyslist[2:])

        if keyslist[0] == "property":
            if keyslist[1] in list(Schema.terms.keys()):
                property = Schema.terms[keyslist[1]].get_property()
                self.fields["properties"].append(property)

                if not keyslist[2].isnumeric():
                    return property.get_store_object(root, keyslist[2:])
                else:
                    return property.get_store_object(root, keyslist[3:])

        if keyslist[0] in list(Schema.terms.keys()):
            property = Schema.terms[keyslist[0]].get_property()
            self.fields["properties"].append(property)

            if not keyslist[1].isnumeric():
                return property.get_store_object(root, keyslist[1:])
            else:
                return property.get_store_object(root, keyslist[2:])


class Emission(Schema):
    def __init__(self):
        super().__init__()
        self.fields["type"] = "Emission"
        self.fields["term"] = None
        self.fields["description"] = ""
        self.fields["amount"] = []
        self.fields["relDays"] = []
        self.fields["sd"] = None
        self.fields["sdDefinition"] = ""
        self.fields["properties"] = []
        self.fields["method"] = None
        self.fields["methodDescription"] = ""
        self.fields["methodTier"] = None
        self.fields["source"] = None

        self.source = None

    def add_object(self, root, keyslist):

        if keyslist[0] == "source":
            if self.source is None:
                self.source = Source()
                self.fields["source"] = self.source.get_reference()

            if not keyslist[1].isnumeric():
                return self.source.get_store_object(root, keyslist[1:])
            else:
                return self.source.get_store_object(root, keyslist[2:])

        if keyslist[0] == "property":
            if keyslist[1] in list(Schema.terms.keys()):
                property = Schema.terms[keyslist[1]].get_property()
                self.fields["properties"].append(property)

                if not keyslist[2].isnumeric():
                    return property.get_store_object(root, keyslist[2:])
                else:
                    return property.get_store_object(root, keyslist[3:])

        if keyslist[0] in list(Schema.terms.keys()):
            property = Schema.terms[keyslist[0]].get_property()
            self.fields["properties"].append(property)

            if not keyslist[1].isnumeric():
                return property.get_store_object(root, keyslist[1:])
            else:
                return property.get_store_object(root, keyslist[2:])


class Site(Schema):
    record = []
    counter = 0

    def __init__(self):
        super().__init__()
        self.fields["type"] = "Site"
        self.fields["id"] = "site/" + self.get_unique_identifier(Site.record)
        self.fields["name"] = ""
        self.fields["organisation"] = None
        self.fields["siteType"] = ""
        self.fields["country"] = ""
        self.fields["region"] = ""
        self.fields["subRegion"] = ""
        self.fields["subSubRegion"] = ""
        self.fields["latitude"] = None
        self.fields["longitude"] = None
        self.fields["boundary"] = None
        self.fields["area"] = None
        self.fields["startDate"] = None
        self.fields["endDate"] = None
        self.fields["measurements"] = []
        self.fields["infrastructure"] = []
        self.fields["dataPrivate"] = None

    def add_object(self, root, keyslist):

        if keyslist[0] == "measurement":
            if keyslist[1] in list(Schema.terms.keys()):
                measurement = Schema.terms[keyslist[1]].get_measurement()
                self.fields["measurements"].append(measurement)

                if not keyslist[2].isnumeric():
                    return measurement.get_store_object(root, keyslist[2:])
                else:
                    return measurement.get_store_object(root, keyslist[3:])

    def check_duplicate(self):
        for site in Schema.sites.values():
            if self.check_coordinates(site) and self.check_measurements(site):
                self.fields["id"] = site.fields["id"]
                return site
        return None

    def check_coordinates(self, site):
        if (self.fields["latitude"] == site.fields["latitude"] and
                self.fields["latitude"] == site.fields["latitude"]):
            return True
        else:
            return False

    def check_measurements(self, site):
        conditions = []
        for object_measurement, schema_measurement in zip(self.fields["measurements"], site.fields["measurements"]):
            if self.check_measurement(object_measurement, schema_measurement):
                conditions.append(True)
            else:
                return False

        if all(conditions):
            return True
        else:
            False

    def check_measurement(self, object_measurement, schema_measurement):
        if (object_measurement.fields["method"] == schema_measurement.fields["method"] and
                object_measurement.fields["methodDescription"] == schema_measurement.fields["methodDescription"] and
                object_measurement.fields["date"] == schema_measurement.fields["date"] and
                object_measurement.fields["time"] == schema_measurement.fields["time"] and
                object_measurement.fields["units"] == schema_measurement.fields["units"] and
                object_measurement.fields["amount"] == schema_measurement.fields["amount"]):
            return True
        else:
            return False

    def format(self):
        if self.fields["name"] == "":
            self.fields["name"] = self.fields["siteType"] + "-" + str(Site.counter)
            Site.counter = Site.counter + 1

    def update_schema(self):
        Schema.sites[self.fields["id"]] = self
        new_objects = [self]

        return new_objects


class Measurement(Schema):
    def __init__(self):
        super().__init__()
        self.fields["type"] = "Measurement"
        self.fields["term"] = None
        self.fields["method"] = None
        self.fields["methodDescription"] = ""
        self.fields["date"] = ""
        self.fields["time"] = ""
        self.fields["units"] = ""
        self.fields["amount"] = None
        self.fields["sd"] = None
        self.fields["source"] = None


class Infrastructure(Schema):
    record = []

    def __init__(self):
        super().__init__()
        self.fields["type"] = "Infrastructure"
        self.fields["id"] = "infrastructure/" + self.get_unique_identifier(Infrastructure.record)
        self.fields["name"] = ""
        self.fields["term"] = None
        self.fields["startDate"] = None
        self.fields["endDate"] = None
        self.fields["lifespan"] = None
        self.fields["source"] = None
        self.fields["inputs"] = []


class Organisation(Schema):
    record = []

    def __init__(self):
        super().__init__()
        self.fields["type"] = "Organisation"
        self.fields["id"] = "organisation/" + self.get_unique_identifier(Organisation.record)
        self.fields["name"] = ""
        self.fields["streetAddress"] = ""
        self.fields["city"] = ""
        self.fields["addressRegion"] = ""
        self.fields["country"] = ""
        self.fields["postOfficeBoxNumber"] = ""
        self.fields["postalCode"] = ""
        self.fields["startDate"] = None
        self.fields["endDate"] = None
        self.fields["dataPrivate"] = None


class Inventory(Schema):
    record = []

    def __init__(self):
        super().__init__()
        self.fields["type"] = "Inventory"
        self.fields["id"] = "inventory/" + self.get_unique_identifier(Inventory.record)
        self.fields["name"] = ""
        self.fields["version"] = None
        self.fields["cycle"] = None
        self.fields["product"] = None
        self.fields["functionalUnitMeasure"] = ""
        self.fields["functionalUnitQuantity"] = None
        self.fields["inventory"] = []
        self.fields["dataPrivate"] = None


class Source(Schema):
    record = []

    def __init__(self):
        super().__init__()
        self.fields["type"] = "Source"
        self.fields["id"] = "source/" + self.get_unique_identifier(Source.record)
        self.fields["name"] = ""
        self.fields["bibliography"] = None
        self.fields["metaAnalysisBibliography"] = None
        self.fields["doiHESTIA"] = ""
        self.fields["uploadDate"] = None
        self.fields["uploadBy"] = None
        self.fields["validationDate"] = None
        self.fields["validationBy"] = []
        self.fields["design"] = []

        self.bibliography = None

    def add_object(self, root, keyslist):

        if keyslist[0] == "bibliography":
            if self.bibliography is None:
                self.bibliography = Bibliography()
                self.fields["bibliography"] = self.bibliography.get_reference()

            if not keyslist[1].isnumeric():
                return self.bibliography.get_store_object(root, keyslist[1:])
            else:
                return self.bibliography.get_store_object(root, keyslist[2:])

    def format(self):
        self.fields["bibliography"] = self.bibliography.get_reference()
        if self.fields["name"] == "":
            self.fields["name"] = self.fields["bibliography"].fields["name"]

    def update_schema(self):
        Schema.sources[self.fields["name"]] = self
        new_objects = []

        if not self.bibliography is None:
            new_objects.extend(self.bibliography.update_schema())

        new_objects.extend([self])

        return new_objects

        self.bibliography.update_chema()

    def check_duplicate(self):
        for source in Schema.sources.values():
            if (self.fields["name"] == source.fields["name"]):
                self.fields["id"] = source.fields["id"]
                return source
        return None


class Bibliography(Schema):
    record = []

    def __init__(self):
        super().__init__()
        self.fields["type"] = "Bibliography"
        self.fields["id"] = "bibliography/" + self.get_unique_identifier(Bibliography.record)
        self.fields["name"] = ""
        self.fields["documentDOI"] = ""
        self.fields["arxivID"] = ""
        self.fields["isbn"] = ""
        self.fields["issn"] = ""
        self.fields["scopus"] = ""
        self.fields["ssrn"] = ""
        self.fields["mendeleyID"] = ""
        self.fields["title"] = ""
        self.fields["documentType"] = ""
        self.fields["authors"] = []
        self.fields["outlet"] = ""
        self.fields["year"] = None
        self.fields["volume"] = None
        self.fields["issue"] = None
        self.fields["chapter"] = ""
        self.fields["pages"] = ""
        self.fields["publisher"] = ""
        self.fields["city"] = ""
        self.fields["editors"] = []
        self.fields["institutionPub"] = None
        self.fields["websites"] = []
        self.fields["dateAccessed"] = None
        self.fields["abstract"] = ""

    def update_schema(self):
        Schema.bibliographies[self.fields["name"]] = self
        new_objects = [self]

        return new_objects


class Actor(Schema):
    record = []

    def __init__(self):
        super().__init__()
        self.fields["type"] = "Actor"
        self.fields["id"] = "actor/" + self.get_unique_identifier(Actor.record)
        self.fields["name"] = ""
        self.fields["firstName"] = ""
        self.fields["lastName"] = ""
        self.fields["orcid"] = ""
        self.fields["scopusID"] = ""
        self.fields["primaryInstitution"] = ""
        self.fields["city"] = ""
        self.fields["country"] = ""
        self.fields["email"] = ""
        self.fields["website"] = ""


class Ref(Schema):
    def __init__(self):
        super().__init__()
        self.fields["type"] = ""
        self.fields["id"] = ""
        self.fields["name"] = ""
        self.fields["units"] = ""


class Store():
    def __init__(self, dest_folder):
        self.objects_generated = []
        self.identifiers = []
        self.path = dest_folder
        self.objects_imported = {}
        self.columns = {}

    def generate_files(self):
        cluster_output = []
        for object in self.objects_generated:
            individual_output = json.loads(object.write())
            cluster_output.append(individual_output)

        sliced_cluster_output = [cluster_output[i:i + 1000] for i in range(0, len(cluster_output), 1000)]

        for slice in sliced_cluster_output:
            part = str((sliced_cluster_output.index(slice) + 1) * 1000)
            dest_file = f"{self.path}/{self.term}-{self.termType}-{part}.json"
            with open(dest_file, 'w') as the_file:
                the_file.write(json.dumps(self.format_output(slice), indent=2))

    def format_output(self, slice):
        final_format = {}
        final_format["nodes"] = []
        for element in slice:
            final_format["nodes"].append(element)

        return final_format

    def get_objects(self):
        return self.objects_generated

    def generate_store_object(self):
        pass

    def get_files(self, path):
        terms = {}
        if os.path.isdir(path):
            for file in os.listdir(path):
                s = open(path + file, 'r').read()
                individual_term = eval(s)
                terms[individual_term["name"]] = individual_term["id"]

        return terms

    def import_spreadsheet(self, spreadsheet):
        dataframe_original = pd.read_excel(spreadsheet)
        if "-" in dataframe_original.columns:
            del dataframe_original["-"]
        root_schema_objects_dict = {dataframe_original.at[0, element]: element.split(".")[0] for element in
                                    dataframe_original.columns}
        dataframe_original.columns = list(dataframe_original.iloc[0, :].values)
        dataframe = dataframe_original.drop(0)
        dataframe = self.delete_helpers(dataframe)

        if not dataframe.isnull().values.all():
            rows = [i for i in range(0, dataframe.shape[0])]
            for row in rows:
                store_object = self.generate_store_object()
                store_object.build_map(root_schema_objects_dict, dataframe.columns)

                for column in dataframe.columns:
                    if not store_object.isnan(column):
                        vlaue = dataframe.at[row, column]
                        store_object.update_value(column, vlaue)

                store_object.format()
                new_objects = store_object.update_schema()
                self.objects_generated.extend(new_objects)


class Terms(Store):
    def __init__(self, dest_folder: str, term_type: str):
        super().__init__(dest_folder)
        self.term_type = term_type

    def define(self, id):
        [self.term, self.termType] = id.split("/")

    def delete_helpers(self, dataframe):
        dataframe = dataframe.reset_index(drop=True)
        return dataframe

    def generate_store_object(self):
        return Term(self.termType)

    def convert(self, filepath: str):
        self.define(f"term/{self.term_type}")
        self.import_spreadsheet(filepath)
        self.generate_files()


CONVERTER = {
    NodeType.Term: Terms
}


def convert(dest_folder: str, node_type: NodeType, filepath: str, *args):
    return CONVERTER[node_type](dest_folder, *args).convert(filepath) if node_type in CONVERTER else None
