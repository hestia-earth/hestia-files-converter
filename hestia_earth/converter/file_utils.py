import os


def mkdirs(dest: str):
    if not os.path.isdir(dest):
        os.makedirs(dest)


def has_ext(ext: str) -> bool:
    def compare(file: str): return os.path.splitext(file)[1] == ext
    return compare
