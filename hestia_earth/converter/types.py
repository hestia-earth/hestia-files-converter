from enum import Enum


class NodeType(Enum):
    Actor = 'Actor',
    Bibliography = 'Bibliography'
    Cycle = 'Cycle'
    Inventory = 'Inventory'
    Organisation = 'Organisation'
    Site = 'Site'
    Source = 'Source'
    Term = 'Term'
