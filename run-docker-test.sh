#!/bin/sh
docker build -t hestia-files-converter:test -f tests/Dockerfile .
docker run --rm -v ${PWD}/coverage:/app/coverage -v ${PWD}/data:/app/data hestia-files-converter:test
