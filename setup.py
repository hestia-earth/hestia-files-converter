from setuptools import find_packages, setup


with open("README.md", "r") as fh:
    long_description = fh.read()


setup(
    name='hestia_earth.converter',
    packages=find_packages(),
    version='0.0.0',
    description='Hestia Files Converter library',
    long_description=long_description,
    long_description_content_type="text/markdown",
    author='Guillaume Royer',
    author_email='guillaumeroyer.mail@gmail.com',
    license='GPL-3.0-or-later',
    url='https://gitlab.com/hestia-earth/hestia-files-converter',
    keywords=['hestia', 'converter', 'excel', 'csv', 'json'],
    classifiers=[],
    install_requires=[
        'pandas==1.0.3',
        'xlrd==1.2.0'
    ],
    python_requires='>=3'
)
