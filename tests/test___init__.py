import unittest
from unittest.mock import patch
from .utils import fixtures_path

from hestia_earth.converter import Converter, NodeType


class TestConverter(unittest.TestCase):
    @patch('hestia_earth.converter.convert')
    def test_convert_excel(self, mock_convert):
        filepath = f"{fixtures_path}/fuel.xlsx"
        Converter('./data').convert(NodeType.Term, filepath, 'fuel')
        mock_convert.assert_called_once_with('./data', NodeType.Term, filepath, 'fuel')


if __name__ == '__main__':
    unittest.main()
