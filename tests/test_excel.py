import unittest
from .utils import fixtures_path

from hestia_earth.converter import NodeType
from hestia_earth.converter.excel import convert


class TestExcel(unittest.TestCase):
    def test_convert_terms(self):
        convert('./data', NodeType.Term, f"{fixtures_path}/liveAnimal.xlsx", 'liveAnimal')
        convert('./data', NodeType.Term, f"{fixtures_path}/property.xlsx", 'property')
