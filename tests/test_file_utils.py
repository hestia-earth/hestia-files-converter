import unittest
from unittest.mock import patch

from hestia_earth.converter.file_utils import has_ext, mkdirs


class FakeRequest:
    def iter_content(self, _n):
        return ['test']


class TestFileUtils(unittest.TestCase):
    def test_has_ext(self):
        self.assertTrue(has_ext('.txt')('test.txt'))
        self.assertFalse(has_ext('.txt')('test.md'))

    @patch('os.makedirs')
    @patch('os.path.isdir')
    def test_mkdirs_exists(self, isdir_mock, makedirs_mock):
        isdir_mock.return_value = True
        mkdirs('test')
        makedirs_mock.assert_not_called()

    @patch('os.makedirs')
    @patch('os.path.isdir')
    def test_mkdirs_not_exist(self, isdir_mock, makedirs_mock):
        isdir_mock.return_value = False
        mkdirs('test')
        makedirs_mock.assert_called_once()


if __name__ == '__main__':
    unittest.main()
